# encoding: utf-8
$: << Dir.getwd
$: << Dir.getwd + "/OpenRGSS/lib/openrgss"
$: << Dir.getwd + "/OpenRGSS-RPGMaker/lib/rpg"

# require openrgss
require "table"
require "color"
require "tone"
# require rpgmaker
require "rpgxp/map"
require "rpgxp/mapinfo"
require "rpgxp/event"
require "rpgxp/event_page"
require "rpgxp/event_page_condition"
require "rpgxp/event_page_graphic"
require "rpgxp/eventcommand"
require "rpgxp/moveroute"
require "rpgxp/movecommand"
require "rpgxp/actor"
require "rpgxp/class"
require "rpgxp/class_learning"
require "rpgxp/skill"
require "rpgxp/item"
require "rpgxp/weapon"
require "rpgxp/armor"
require "rpgxp/enemy"
require "rpgxp/enemy_action"
# require "rpgxp/troop"
require "rpgxp/troop_member"
require "rpgxp/troop_page"
require "rpgxp/troop_page_condition"
require "rpgxp/state"
require "rpgxp/animation"
require "rpgxp/animation_frame"
require "rpgxp/animation_timing"
require "rpgxp/tileset"
require "rpgxp/commonevent"
require "rpgxp/system"
require "rpgxp/system_words"
require "rpgxp/system_testbattler"
require "rpgxp/audiofile"
# require 'rpgxp/sprite'
require "rpgxp/weather"
require "rpgxp/cache"

require "rpgxp-patch"

module RGSS
  def load_data(filename)
    File.open(filename, "rb") { |f|
      obj = Marshal.load(f)
    }
  end

  def save_data(obj, filename)
    File.open(filename, "wb") { |f|
      Marshal.dump(obj, f)
    }
  end
end

include RGSS
