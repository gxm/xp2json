# Introduction

Marshal load rgss data -> save in json format

## step

1. copy data folder here
2. git clone openrgss code
3. load *.rxdata file
4. transfer to json format

## run

Copy data/ into folder `data/`, run `ruby xp2json.rb`, generate json data in folder `json/`

```bash
git clone https://github.com/zh99998/OpenRGSS.git --depth=1
git clone https://github.com/zh99998/OpenRGSS-RPGMaker.git --depth=1
mkdir json, json2data
ruby xp2json.rb
```

## convert json to xpdata

Change json data, run `ruby json2xp.rb`, generate ruby marshal data in folder `json2data/`

## bugs

1. A little bigger than origin data


## scripts

Run `ruby rpgxp-scripts.rb`, generate all files in F11.