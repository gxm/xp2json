require_relative "rpgxp-data"
require "zlib"

script = load_data("data/scripts.rxdata")

script.each do |id, title, data|
  fn = title.gsub(/[^\w]/, "")
  File.open("script/%s.rb" % fn, "w") do |f|
    f << Zlib::Inflate.inflate(data).gsub(/\r\n/, "\n")
  end
end

File.open("script/RGSS_Start.rb", "w") do |f|
  script.each do |id, title, data|
    fn = title.gsub(/[^\w]/, "")
    f << "require '%s'\n" % fn
  end
end
