require_relative "rpgxp-json"

if __FILE__ == $0
  Dir.glob("data/*.rxdata").each do |fn|
    next if fn =~ /[Ss]cripts/ # exclude scripts

    json_fn = format("json/%s.json", fn.split("/")[-1].split(".")[0])
    obj = load_data(fn)
    File.open(json_fn, "w") do |f|
      f << fade(obj).to_json
    end
  end
end
