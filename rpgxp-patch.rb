module RPG
  class Troop
    def initialize
      @id = 0
      @name = ""
      @members = [RPG::Troop::Member.new]
      @pages = [RPG::Troop::Page.new]
    end

    attr_accessor :id
    attr_accessor :name
    attr_accessor :members
    attr_accessor :pages
  end
end

module RPG
  class Animation
    alias _old_initialize initialize

    def initialize
      _old_initialize
      @timings = [RPG::Animation::Timing.new]
    end
  end

  class System
    alias _old_initialize initialize

    def initialize
      _old_initialize
      @test_battlers = [RPG::System::TestBattler.new]
    end
  end

  class Class
    alias _old_initialize initialize

    def initialize
      _old_initialize
      @learnings = [RPG::Class::Learning.new]
    end
  end
end
