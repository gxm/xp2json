require_relative "rpgxp-json"

if __FILE__ == $0
  # load globals
  globals = {
    "Armors" => RPG::Armor,
    "CommonEvents" => RPG::CommonEvent,
    "Items" => RPG::Item,
    "Skills" => RPG::Skill,
    "States" => RPG::State,
    "System" => RPG::System,
    "Troops" => RPG::Troop,
    "Weapons" => RPG::Weapon,
    "Actors" => RPG::Actor,
    "Classes" => RPG::Class,
    "Enemies" => RPG::Enemy,
    "Tilesets" => RPG::Tileset,
    "Animations" => RPG::Animation,
  }

  globals.each_pair do |fn, klass|
    path = format("json/%s.json", fn)
    next if !File.exist?(path)
    json = File.open(path, "r") do |f|
      JSON.load(f)
    end
    data = render(klass, json)
    save_data(data, format("json2data/%s.rxdata", fn))
  end

  # load MapInfos
  json = File.open("json/MapInfos.json", "r") do |f|
    JSON.load(f)
  end

  data = {}
  json.each_pair do |k, v|
    data[k.to_i] = render(RPG::MapInfo, v)
  end
  save_data(data, "json2data/MapInfos.rxdata")

  # load maps
  Dir.glob("json/Map*.json").each do |fn|
    next if fn =~ /MapInfos/
    json = File.open(fn, "r") do |f|
      JSON.load(f)
    end
    w, h = json["width"], json["height"]
    data = _render(RPG::Map.new(w, h), json)
    save_data(data, "json2data/" + fn.split("/")[1].split(".")[0] + ".rxdata")
  end
end
